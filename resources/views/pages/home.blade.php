@extends('layouts.app')
@section('title','Home Page')
@section('home','uk-active')
@section('content')
{{-- Top navs --}}
<div class="uk-flex uk-position-top-right mt-6 mr-6 text-white rounded font-bold bg-black opacity-75 p-2 md:hidden">
    <div><a href="#about" uk-scroll class=" hover:text-red-500 mr-6" style="text-decoration: none">About</a></div>
    <div><a href="#our-product" uk-scroll class=" hover:text-red-500 mr-0" style="text-decoration: none">Our products</a></div>
    {{-- <div><a href="#coming-soon" uk-scroll class=" hover:text-black" style="text-decoration: none">Other products</a></div> --}}
</div>
<!-- Welocome card -->
<section id="about">
    <div class="uk-card uk-card-body uk-card-small uk-card-default">
        <div class="uk-grid-small uk-child-width-1-2@m" uk-grid
            uk-scrollspy="cls: uk-animation-fade; target: .content; delay: 500; repeat: false">
            <!-- Left -->
            <div class="m-auto md:text-center content">
                <div class="text-3xl font-bold uppercase">Welcome to <span class="text-red-500">Lak fortune
                        Australia</span></div>
                <br>
                {{-- <p class="font-bold text-lg">We assured the quality of our products and promise our customers a
                    areliable after
                    sale service and we proved them our
                    strengths and capabilities</p> --}}
                <br>
                <p class="text-lg">We are happy to introduce ourselves as Melbourne’s
                    specialist distributor
                    of predominant quality tamper evident security products and
                    providing highly customer satisfaction to all our clients.
                    We are working on the area’s security solutions Lighting Solutions
                    and Plant and Machinery Solutions. We have supplied security seals
                    for largest conglomerates in all industries We strongly believe in R&D
                    which always rescue us in the technology wave. We rely on latest technology
                    and work in professional ethics. Our success is the customer satisfaction.
                    We are a coherent group working with solid vision and focus on our strengths.</p>
            </div>
            <!-- Right -->
            <div class="md:hidden content">
                <img src="{{asset("assets/images/welcome.webp")}}" height="100%" alt="" >
            </div>
        </div>
    </div>
</section>
{{-- Product cards --}}
<section class="mt-4" id="our-product">
    <div class="text-2xl text-center font-bold bg-white mt-4 mb-4 p-2">Our products</div>
    <div class="uk-grid-small uk-child-width-1-3@m uk-grid-match" uk-grid
        uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 300; repeat: false">

        {{-- Security labels --}}
        <div>
            <div class="uk-card uk-card-default">
                <div class="uk-card-media-top">
                    <img src="{{asset("assets/images/item1-1.webp")}}" uk-img="target: !.uk-card-media-top">
                </div>
                <div class="uk-card-body">
                    <h3 class="text-md font-bold uk-margin-small-bottom	">HIGH SECURITY UTILITY SEALS</h3>
                    <p class="uk-height-max-small uk-overflow-auto">The Twist-Tite Wire Seal is an excellent general
                        purpose wire seal. The Twist-Tite Wire Seal features a one-way ratchet mechanism which
                        prevents
                        the wire from being backed out. The clear acrylic body is engineered to reveal tampering
                        attempts. The Twist-Tite is available in a variety of wire materials in lengths from 6 - 18
                        inches. Imprinting options are available on the body and insert.</p>
                </div>
                <div class="text-center uk-margin">
                    <div class="uk-button uk-button-small bg-red-500 text-white rounded-full"><a href="/itemview">View
                            product</a></div>
                </div>
            </div>
        </div>

        {{-- Barrier seals --}}
        <div>
            <div class="uk-card uk-card-default">
                <div class="uk-card-media-top">
                    <img src="{{asset("assets/images/item2-1.webp")}}" alt="" uk-img="target: !.uk-card-media-top">
                </div>
                <div class="uk-card-body">
                    <h3 class="text-md font-bold uk-margin-small-bottom	">HIGH SECURITY CONTAINER SEALS</h3>
                    <p class="uk-height-max-small uk-overflow-auto">Steel body with ABS coating,mainly used all
                        kinds of
                        containers,trucks,tanks,doors,postal services,courier services,ect. we have various color
                        for
                        bolt seal and support lase printing on bolt seal.like company logo,text,letters,numbers and
                        bar
                        code.</p>

                </div>
                <div class="text-center uk-margin">
                    <div class="uk-button uk-button-small bg-red-500 text-white rounded-full"><a href="/itemview1">View
                            product</a></div>
                </div>
            </div>
        </div>
        {{-- Plastic seal --}}
        <div>
            <div class="uk-card uk-card-default">
                <div class="uk-card-media-top">
                    <img src="{{asset("assets/images/plastic.webp")}}"  uk-img="target: !.uk-card-media-top">
                </div>
                <div class="uk-card-body">
                    <h3 class="text-md font-bold uk-margin-small-bottom	">PLASTIC SEALS</h3>
                    <p class="uk-height-max-small uk-overflow-auto">plastic security seals will help you to secure
                        various kinds of shipments, these indicative seals are not very effect in prevent access to
                        your
                        shipment or cargo but will provide you with an evidence of unauthorized access.
                    </p>
                </div>
                <div class="text-center uk-margin">
                    <div class="uk-button uk-button-small bg-red-500 text-white rounded-full"><a href="/itemview2">View
                            product</a></div>
                </div>
            </div>
        </div>

</section>

{{-- Other products carousel --}}
<!-- <section id="coming-soon">
    <div class="text-2xl text-center font-bold bg-white mt-4 mb-4 p-2">Comming soon</div>
    <div uk-slider autoplay='true' autoplay-interval='3000'>
        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
            <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-4@m uk-child-width-1-1 uk-grid-match"
                uk-grid uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 300; repeat: false">
                <li>
                    <div class="uk-card uk-card-small uk-card-default">
                        <div class="uk-card-media-top">
                            <img src="{{asset("assets/images/tape.webp")}}" uk-img="target: !.uk-card-media-top">
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title">Seal tags</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt.
                            </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-card uk-card-small uk-card-default">
                        <div class="uk-card-media-top">
                            <img src="{{asset("assets/images/item1-2.webp")}}"  uk-img="target: !.uk-card-media-top">
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title">Seal tags</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt.
                            </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-card uk-card-small uk-card-default">
                        <div class="uk-card-media-top">
                            <img src="{{asset("assets/images/item1-3.webp")}}"  uk-img="target: !.uk-card-media-top">
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title">Seal tags</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt.
                            </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-card uk-card-small uk-card-default">
                        <div class="uk-card-media-top">
                            <img src="{{asset("assets/images/item2-2.webp")}}"  uk-img="target: !.uk-card-media-top">
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title">Seal tags</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt.
                            </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-card uk-card-small uk-card-default">
                        <div class="uk-card-media-top">
                            <img src="{{asset("assets/images/item2-1.webp")}}"  uk-img="target: !.uk-card-media-top">
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title">Seal tags</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt.
                            </p>
                        </div>
                    </div>
                </li>
            </ul>

            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
                uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
                uk-slider-item="next"></a>

        </div>

        <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

    </div>
    
</section> -->
{{-- To top button --}}
<section>
    <div class="float-right mt-6">
        <a href="#" uk-scroll ><i
                class="fas fa-chevron-circle-left transform rotate-90 text-red-500 text-4xl"></i></a>
    </div>
</section>
@endsection