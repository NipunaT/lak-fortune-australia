@extends('layouts.app')
@section('title','Item')
@section('maincarousel','hidden')
@section('content')
<!-- Top card section -->

<section>
    <div class="uk-grid-small uk-child-width-1-2@m uk-grid-match" uk-grid
        uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 300; repeat: false">

        <!-- ROW 01 -->
        <!-- Left -->
        <div>
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@m uk-margin" uk-grid>
                <div class="uk-card-media-left uk-cover-container">
                    <img src="{{asset("assets/images/item3-3.webp")}}" alt="" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title">SJ -01</h3>
                        <p>
                        <ul class="uk-list">
                            <li>PP/PE</li>
                            <li>Pull tight locking mechanism</li>
                            <li>Total length: 356mm</li>
                            <li>Tail diameter:4mm</li>
                            <li>Applications: Mail bags, post bags, bank bags, cargo transfer bags, containers, etc.
                            </li>
                            <li>Printing: Thermal stamping or laser printing Company logo or/and name, sequential
                                number, Bar
                                code available</li>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Right -->
        <div>
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                <div class="uk-card-media-left uk-cover-container">
                    <img src="{{asset("assets/images/item3-4.webp")}}" alt="" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title">SJ -02</h3>
                        <p>
                        <ul class="uk-list">
                            <li>PP/PE</li>
                            <li>Pull tight locking mechanism</li>
                            <li>Operationl length: 284mm</li>
                            <li>Tail diameter:3mm</li>
                            <li>Applications: Mail bags, post bags, bank bags, cargo transfer bags,containers, etc</li>
                            <li>Printing: Company logo or/and name, sequential number, Bar code available</li>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- ROW 02 -->
        <!-- Left -->
        <div>
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                <div class="uk-card-media-left uk-cover-container">
                    <img src="{{asset("assets/images/item3-5.webp")}}" alt="" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title">SJ -03</h3>
                        <p>
                        <ul class="uk-list">
                            <li>PP plastic</li>
                            <li>Pull tight locking mechanism</li>
                            <li>Dimension:187mm(L)*11mm(W)</li>
                            <li>Applications: Mail bags, post bags, bank bags, cargo transfer bags, containers, etc</li>
                            <li>Printing: Thermal stamping or laser printing Company logo or/and name, sequential
                                number, Bar
                                code available</li>
                        </ul>

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Right -->
        <div>
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                <div class="uk-card-media-left uk-cover-container">
                    <img src="{{asset("assets/images/item3-6.webp")}}" alt="" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title">SJ -04</h3>
                        <p>
                        <ul class="uk-list">
                            <li>PP plastic with steel insert lock</li>
                            <li>Pull tight locking mechanism</li>
                            <li>Total length: 265mm</li>
                            <li>Operation length:227</li>
                            <li>Tail diameter:2.5mm</li>
                            <li>Lock head:16mm*38mm</li>
                            <li>Applications: Mail bags, post bags, bank bags, cargo transfer bags, containers, etc</li>
                            <li>Printing: Thermal stamping or laser printing Company logo or/and name, sequential
                                number, Bar
                                code available</li>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- ROW 03 -->
        <!-- Left -->
        <div>
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                <div class="uk-card-media-left uk-cover-container">
                    <img src="{{asset("assets/images/item3-1.webp")}}" alt="" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title">SJ -05</h3>
                        <p>
                        <ul class="uk-list">
                            <li>PP plastic with steel insert lock</li>
                            <li>Pull tight locking mechanism</li>
                            <li>Total length: 375mm</li>
                            <li>Operation length:300mm</li>
                            <li>Tail diameter:3.5mm</li>
                            <li>Lock head:23mm*75mm</li>
                            <li>Applications: Mail bags, post bags, bank bags, cargo transfer bags, containers, etc.
                            </li>
                            <li>Printing: Thermal stamping or laser printing Company logo or/and name, sequential number
                                , Bar
                                code available</li>
                        </ul>

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Right -->
        <div>
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                <div class="uk-card-media-left uk-cover-container">
                    <img src="{{asset("assets/images/item3-2.webp")}}" alt="" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title">SJ -06</h3>
                        <p>
                        <ul class="uk-list">
                            <li>PP plastic with steel insert lock</li>
                            <li>Pull tight locking mechanism</li>
                            <li>Total length: 355mm</li>
                            <li>Strip width:6mm</li>
                            <li>Lock head:23mm*75mm</li>
                            <li>Applications: Mail bags, post bags, bank bags, cargo transfer bags, containers, etc</li>
                            <li>Printing: Thermal stamping or laser printing Company logo or/and name, sequential
                                number, Bar
                                code available</li>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Description section -->
<section>
    <div class="uk-card uk-card-default uk-card-small uk-card-body mt-4">
        <div class="uk-text-lead text-center m-2">Description</div>
        <div class="uk-child-width-1-3@m justify-center" uk-grid
            uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 500" uk-lightbox="animation: slide">
            <div>
                <a class="uk-inline" href="{{asset("assets/images/des3-1.webp")}}" data-caption="Image 1">
                    <img src="{{asset("assets/images/des3-1.webp")}}" alt="" class="border border-black">
                </a>
            </div>
            <div>
                <a class="uk-inline" href="{{asset("assets/images/des3-2.webp")}}" data-caption="Image 2">
                    <img src="{{asset("assets/images/des3-2.webp")}}" alt="" class="border border-black">
                </a>
            </div>
        </div>
</section>
@endsection