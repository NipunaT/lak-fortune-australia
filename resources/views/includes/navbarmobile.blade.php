<nav class="bg-white sticky z-50 inset-x-0 bottom-0 shadow-md mdup:hidden"  uk-navbar>
    <!-- Center -->
    <div class="uk-navbar-center">
        <ul class="uk-navbar-nav justify-around">
            <li class=@yield('home')><a href="/"><i class="fas fa-home text-xl mr-1"></i><span
                        class="md:hidden">Home</span></a></li>
            <li>
                <a href="#"><i class="fas fa-boxes text-xl mr-1"></i><span class="md:hidden">Products</span></a>
                <div uk-dropdown='pos: top-left'>
                    <ul class="uk-list md:uk-list-striped">
                        <li class="text-center md:text-left">
                            <a href="#">
                                <img src="{{asset("assets/images/item1-1.webp")}}" alt="" width="50px">High security utility seals
                            </a>
                        </li>
                        <li class="text-center md:text-left">
                            <a href="#">
                                <img src="{{asset("assets/images/plastic.webp")}}" alt="" width="50px">Plastic seals
                            </a>
                        </li>
                        <li class="text-center md:text-left">
                            <a href="#">
                                <img src="{{asset("assets/images/item2-2.webp")}}" alt="" width="50px">High security container seals
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class=@yield('about')><a href="/aboutus"><i class="fas fa-users text-xl mr-1"></i><span class="md:hidden">About
                        us</span></a>
            </li>
            <li class=@yield('contact')><a href="/contactus"><i class="fas fa-address-card text-xl mr-1"></i><span class="md:hidden">Contact
                        us</span></a>
            </li>
        </ul>
    </div>
</nav>