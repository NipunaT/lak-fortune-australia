<?php

use Illuminate\Support\Facades\Route;
use App\Mail\MessageMail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});
Route::get('/aboutus', function () {
    return view('pages.aboutus');
});
Route::get('/contactus', function () {
    return view('pages.contactus');
});
Route::get('/itemview', function () {
    return view('pages.itemview');
});
Route::get('/itemview1', function () {
    return view('pages.itemview1');
});
Route::get('/itemview2', function () {
    return view('pages.itemview2');
});
route::post('mail','maillcontroller@message')->name('mail.message');
route::post('quotemail/{Item}','maillcontroller@quote')->name('mail.quote');
route::post('subscribe','subscribecontroller@subscribe');



Route::get('/email', function () {
    
    return new MessageMail();
});