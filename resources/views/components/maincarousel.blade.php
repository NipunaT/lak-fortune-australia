<div class="uk-position-relative uk-visible-toggle maincarousel h-full" autoplay='true' autoplay-interval='4000' pause-on-hover='false' tabindex="-1" uk-slideshow='velocity:0.75 ; animation: push'>

    <ul class="uk-slideshow-items ">
        <li>
            <img src="{{asset("assets/images/container.webp")}}"  alt="" uk-cover uk-img="target: !.uk-slideshow-items">
             <div class="uk-position-center w-full  bg-opacity-75 uk-text-center bg-black">
                <h3 class="md:text-3xl mdup:text-6xl  text-white"  uk-slider-parallax="x: 1000,-1000">Lak Fortune Australia</h3>
                <p class=" font-bold text-white mb-4"
                uk-slider-parallax="x: 2000,-2000">Security seals for Containers</p>
            </div>
        </li>
        <li>
            <img src="{{asset("assets/images/tanker.webp")}}"  alt="" uk-cover uk-img="target: !.uk-slideshow-items">
             <div class="uk-position-center w-full bg-opacity-75 uk-text-center bg-black">
                <h3 class="md:text-3xl mdup:text-6xl  text-white"  uk-slider-parallax="x: 1000,-1000">Lak Fortune Australia</h3>
                <p class=" font-bold text-white mb-4"
                uk-slider-parallax="x: 2000,-2000">Security seals for tankers</p>
            </div>
        </li>
        <li>
            <img src="{{asset("assets/images/meter.webp")}}"  alt="" uk-cover uk-img="target: !.uk-slideshow-items">
             <div class="uk-position-center w-full bg-opacity-75 uk-text-center bg-black">
                <h3 class="md:text-3xl mdup:text-6xl  text-white"  uk-slider-parallax="x: 1000,-1000">Lak fortune Australia</h3>
                <p class=" font-bold text-white mb-4"
                uk-slider-parallax="x: 2000,-2000">Security seals for meters</p>
            </div>
        </li>
    </ul>

    {{-- <a class="uk-position-center-left uk-hidden-hover opacity-25 ml-10" href="#" uk-slideshow-item="previous">
        <i class="fas fa-chevron-circle-left text-red-500 text-4xl"></i></a>
    <a class="uk-position-center-right uk-hidden-hover opacity-25 mr-10" href="#" uk-slideshow-item="next">
        <i class="fas fa-chevron-circle-right text-red-500 text-4xl"></i>
    </a> --}}

</div>