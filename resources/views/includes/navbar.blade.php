<nav class="bg-white sticky z-50 inset-x-0 top-0 mb-4 shadow-md" uk-navbar>
    <!-- Left -->
    <div class="uk-navbar-left">
        <ul class="uk-navbar-nav">
            <li class="uk-active"><a href="/">
                    <div class="text-xl text-red-500 font-bold">Lak fortune Australia</div>
                </a></li>
        </ul>
    </div>
    <!-- Right -->
    <div class="uk-navbar-right md:hidden">
        <ul class="uk-navbar-nav">
            <li class=@yield('home')><a href="/"><i class="fas fa-home md:text-sm mr-1"></i>Home</a></li>
            <li>
                <a href="#"><i class="fas fa-boxes md:text-sm mr-1"></i>Products</a>
                <div class="uk-navbar-dropdown">
                    <ul class="uk-list md:uk-list-striped">
                        <li class="text-center md:text-left">
                            <a href="/itemview">
                                <img src="{{asset("assets/images/item1-1.webp")}}" alt="" class="md:hidden m-auto"
                                    width="50px">High security utility seals
                            </a>
                        </li>
                        <li class="text-center md:text-left">
                            <a href="/itemview2">
                                <img src="{{asset("assets/images/item3-1.webp")}}" alt="" class="md:hidden m-auto"
                                    width="50px">Plastic seals
                            </a>
                        </li>
                        <li class="text-center md:text-left">
                            <a href="/itemview1">
                                <img src="{{asset("assets/images/item2-1.webp")}}" alt="" class="md:hidden m-auto"
                                    width="50px">High security container seals
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class=@yield('about')><a href="/aboutus"><i class="fas fa-users md:text-sm mr-1"></i>About
                        us</a>
            </li>
            <li class=@yield('contact')><a href="/contactus"><i class="fas fa-address-card md:text-sm mr-1"></i>Contact
                        us</a>
            </li>
        </ul>
    </div>
</nav>