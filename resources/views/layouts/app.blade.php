<!DOCTYPE html>
<html lang="en" style="background-color: #efefef">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.4/dist/css/uikit.min.css" />
    {{-- Tailwild --}}
    <link rel="stylesheet" href="./css/main.css">
    {{-- Fontawesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <title>@yield('title')</title>
    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 5px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #f56565;
            border-radius: 10px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }
    </style>
</head>

<body>

    {{-- Maincarousel --}}

    <div class="@yield('maincarousel')">
        @include('components.maincarousel')
    </div>
    {{-- Includes --}}
    @include('includes.navbar')
    {{-- content --}}
    <div class="uk-container uk-container-large">
        @yield('content')
    </div>
    {{-- Footer --}}
    @include('includes.footer')
    {{-- Mobile navbar --}}
    @include('includes.navbarmobile')
    <!-- UIkit JS -->

    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.4/dist/js/uikit.min.js" defer></script>
</body>

</html>