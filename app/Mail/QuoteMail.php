<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QuoteMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $data;

    public function __construct($data_aray)
    {
        $this->data=$data_aray;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $x=$this->data;

        return $this->markdown('emails.quote')
                    ->with([
                        'item' => $x['item'],
                        'email' => $x['email'],
                        'message' => $x['message'],
                    ]);
    }
}
