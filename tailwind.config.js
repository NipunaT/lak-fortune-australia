module.exports = {
    purge: [],
    theme: {
        extend: {},
        screens: {
            'xl': { 'max': "1279px" },
            'lg': { 'max': "1023px" },
            'md': { 'max': "960px" },
            'sm': { 'max': "639px" },
            'xlup': { 'min': "1279px" },
            'lgup': { 'min': "1023px" },
            'mdup': { 'min': "960px" },
            'smup': { 'min': "639px" }
        }
    },
    variants: {},
    plugins: []
};
