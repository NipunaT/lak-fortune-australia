@component('mail::message')
# This person needs to contact Lak Fortune Australia

Contact Details

Email:{{$email}}<br>
First name:{{$fname}}<br>
Last name:{{$lastname}}<br>
Telephone:{{$telephone}}<br>
State:{{$state}}<br>
Poste code:{{$post_code}}<br>
Country:{{$Country}}<br>
Message:{{$message}}




Thanks,<br>
{{ config('app.name') }}
@endcomponent
