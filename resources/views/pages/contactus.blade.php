@extends('layouts.app')
@section('title','Contact us')
@section('contact','uk-active')
@section('maincarousel','hidden')
@section('content')
<div class="uk-grid-small uk-child-width-1-2@m uk-grid-match" uk-grid uk-grid
    uk-scrollspy="cls: uk-animation-slide-top; target: .uk-card; delay: 500; repeat: false">
    <!-- Left    -->
    <div class="md:order-2">
        <div class="uk-card uk-card-small uk-card-default uk-card-body">
            <form class="uk-form-stacked" action="{{route('mail.message')}}" method="post">

                @if (Session::has('sucess'))
                        <!-- Alert successfull -->
                    <div class="uk-alert-primary text-center" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        <p>Message sent successfuly, will contact you soon.</p>
                    </div>
        
                {{-- @else
                    <!-- Alert error -->
                    <div class="uk-alert-danger text-center" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        <p>Sorry Quote NOT Sent !</p>
                    </div> --}}
                @endif
                <!-- Email -->
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">Email</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" name="email" id="form-stacked-text" type="email" placeholder="Email">
                    </div>
                </div>
                <!-- Company name -->
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">Company name</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" name="Company_name" id="form-stacked-text" type="text" placeholder="Company name">
                    </div>
                </div>
                <!-- First name -->
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">First name</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" name="fname" id="form-stacked-text" type="text" placeholder="First name">
                    </div>
                </div>
                <!-- Last name -->
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">Last name</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" name="lname" id="form-stacked-text" type="text" placeholder="Last name">
                    </div>
                </div>
                <!-- Tel. -->
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">Telephone</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" name="telephone" id="form-stacked-text" type="tel" placeholder="Telephone">
                    </div>
                </div>
                <!-- State -->
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">State</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" name="state" id="form-stacked-text" type="text" placeholder="State">
                    </div>
                </div>
                <!-- Post code -->
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">Post code</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" name="post_code" id="form-stacked-text" type="text" placeholder="Post code">
                    </div>
                </div>
                <!-- Country -->
                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">Country</label>
                    <div class="uk-margin">
                        <select class="uk-select" name="Country">
                            <option>Australia</option>
                        </select>
                    </div>
                </div>
                <!-- Message -->

                <div class="uk-margin">
                    <label class="uk-form-label" for="form-stacked-text">Message</label>
                    <textarea class="uk-textarea" name="message" rows="5" placeholder="Message"></textarea>
                </div>
                
                {{-- Send message button --}}

                <button type="submit"
                    class="rounded-full hover:shadow-lg hover:bg-blue-500 hover:border-0 hover:text-white border-solid border border-blue-500 text-blue-500 uk-button float-right">Send
                    message
                    <i class="fa fa-paper-plane ml-2"></i></button>
                    @csrf

            </form>
        </div>
    </div>
    <!-- Right -->
    <div class="md:order-1">
        <div class="uk-card uk-card-small uk-card-body uk-card-default">
            <img src="{{asset("assets/images/aboutus.jpg")}}" class="w-100 md:hidden" alt="">
            <div class="text-left mdup:mt-4">
                <div class="text-xl font-bold">Email / Phone</div>
                <p>
                    If you have any questions, we'd be happy to help. Please get in touch with us.<br>
                    <a href="sales@tamperevident.com.au">lakfortuneaustralia@gmail.com</a><br>
                    Ph:+61 (0) 3 8339 4214<br>
                    ABN: 16624967531<br>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection