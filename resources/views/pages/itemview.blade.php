@extends('layouts.app')
@section('title','Item')
@section('maincarousel','hidden')
@section('content')
<!-- Top card section -->
<section>
    <div class="uk-card uk-card-default uk-card-small uk-card-body">
        <div class="uk-grid-small uk-child-width-1-2@m uk-grid-match" uk-grid>
            <!-- left -->
            <div>
                <div class="uk-position-relative" uk-slideshow="animation: fade" autoplay='true'
                    autoplay-interval='3000' hover-on-pause='true'>

                    <ul class="uk-slideshow-items">
                        <li>
                            <img src="{{asset("assets/images/item1-1.webp")}}" alt="" uk-cover>
                        </li>
                        <li>
                            <img src="{{asset("assets/images/item1-2.webp")}}" alt="" uk-cover>
                        </li>
                        <li>
                            <img src="{{asset("assets/images/item1-3.webp")}}" alt="" uk-cover>
                        </li>
                    </ul>
                    <div class="uk-position-bottom-center uk-position-small opacity-25 hover:opacity-100">
                        <ul class="uk-thumbnav">
                            <li uk-slideshow-item="0"><a href="#"><img src="{{asset("assets/images/item1-1.webp")}}"
                                        width="100" alt="" class="border border-black"
class="border border-black"></a>
                            </li>
                            <li uk-slideshow-item="1"><a href="#"><img src="{{asset("assets/images/item1-2.webp")}}"
                                        width="100" alt="" class="border border-black"
class="border border-black"></a>
                            </li>
                            <li uk-slideshow-item="2"><a href="#"><img src="{{asset("assets/images/item1-3.webp")}}"
                                        width="100" alt="" class="border border-black"
class="border border-black"></a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <!-- Right -->
            <div class="md:mt-4">

                @if (Session::has('sucess'))
                <!-- Alert successfull -->
                    <div class="uk-alert-primary text-center" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        <p>Message sent successfuly, will contact you soon.</p>
                    </div>
                @endif
                
                <div class="uk-text-lead uk-text-success uk-margin-bottom">SJ -08</div>
                <p>
                <ul class="uk-list">
                    <li>Electric Meter seal / Water meter seal</li>
                    <li>ISO tested meter seals for utility applications</li>
                    <li>Cable diameter – 0.5mm , 7 strand PVC coated or optional</li>
                    <li>Wire Length can be vary</li>
                    <li>Test certificate for UV protection , Mechanical stability Heat and chemical resistant</li>
                    <li>Company name , logo serial number ,Barcode available</li>
                </ul>

                </p>
                
                <!-- Send quote -->
                <div class="uk-card bg-red-500 text-white uk-card-small uk-card-body md:mt-4">
                    <div class="text-lg font-bold">Get quote</div>
                    <form action="/quotemail/SJ-08" method="POST">
                        <div class="uk-margin">
                            <label>Name</label><br>
                            <div class="uk-inline uk-width-1-1">
                                <a class="uk-form-icon"><i class="fas fa-user"></i></a>
                                <input class="uk-input" name="name" type="text" placeholder="Name">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label>Email</label><br>
                            <div class="uk-inline uk-width-1-1">
                                <a class="uk-form-icon"><i class="fas fa-at"></i></a>
                                <input class="uk-input" name="email" type="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label>Message</label><br>
                            <div class="uk-inline uk-width-1-1">
                                <input class="uk-textarea" rows="5" name="message" type="text" placeholder="Message">
                            </div>
                        </div>
                        <!-- Send btn -->
                        <button
                            class="uk-button uk-button-default text-white rounded-full float-right hover:bg-white hover:border-white" type="submit">
                            Get<i class="fas fa-paper-plane ml-2"></i></button>
                            @csrf
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Description section -->
<section>
    <div class="uk-card uk-card-default uk-card-small uk-card-body mt-4">
        <div class="uk-text-lead text-center m-2">Description</div>
        <div class="uk-child-width-1-3@m justify-center" uk-grid uk-lightbox="animation: slide">
            <div>
                <a class="uk-inline" href="{{asset("assets/images/des1-1.webp")}}" data-caption="Image 1">
                    <img src="{{asset("assets/images/des1-1.webp")}}" alt="" class="border border-black"
class="border border-black">
                </a>
            </div>
        </div>
</section>
@endsection