<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $data;

    public function __construct($data_aray)
    {
        $this->data=$data_aray;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $x=$this->data;


        return $this->markdown('emails.message')
                    ->with([
                        'email' => $x['email'],
                        'fname' => $x['firstname'],
                        'lastname' => $x['lastname'],
                        'telephone' => $x['telephone'],
                        'state' => $x['state'],
                        'post_code' => $x['post_code'],
                        'Country' => $x['Country'],
                        'message' => $x['message'],
                    ]);
    }
}
