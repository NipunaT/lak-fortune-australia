@extends('layouts.app')
@section('title','About us')
@section('about','uk-active')
@section('maincarousel','hidden')
@section('content')

<div class="uk-grid-small uk-child-width-1-2@m uk-grid-match" uk-grid uk-scrollspy="cls: uk-animation-slide-top; target: .uk-card; delay: 500; repeat: false">
    <!-- Left    -->
    <div>
        <div class="uk-card uk-card-small uk-card-default uk-card-body mdup:uk-height-max-large uk-overflow-auto">
            <ul uk-accordion>
                <li  class="uk-open">
                    <a class="uk-accordion-title" href="#">Who we are</a>
                    <div class="uk-accordion-content">
                        <p>Lak Fortune Australia Pty Ltd is an authorised distributor for SJ
                            Enterprises We are working on the areas of Security Solutions, High end
                            Machinery, Internationally renowned LED displays and advertising
                            solutions We assured the quality of our products and promise our
                            customers a reliable after sale service and we proved them our
                            strengths and capabilities. Our strategy is to build long term relationships
                            with customers. We listen to what customers are saying and work with
                            them to resolve any challenges.</p>
                    </div>
                </li>
                <li>
                    <a class="uk-accordion-title" href="#">Shipping policy</a>
                    <div class="uk-accordion-content">
                        <p>Lak Fortune Australia ships all orders by Australia Post. We try our best
                            to ship the same day as your order is received. However, If we are
                            busier than normal we will ship within at least 3 business days of
                            receiving orders.
                            Alternate shipping arrangements can be made by contacting us
                            Shipping fees will vary, depending on size, weight and destination. This
                            may result in the shipping fees being higher than indicated on your
                            invoice. In this case you will be notified by email to request approval
                            prior to charging you for the additional fees. For international orders, Lak
                            Fortune Australia cannot be responsible for any additional fees/ duties
                            applied at customs.</p>
                    </div>
                </li>
                <li>
                    <a class="uk-accordion-title" href="#">Free Samples</a>
                    <div class="uk-accordion-content">
                        <p>If you’re a first-time customer, we want you to put our products to the
                            test before you buy. That’s why we happily provide free samples of any
                            of our products for you to try out.</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- Right -->
    <div class="md:hidden">
        <div class="uk-card uk-card-small uk-card-body uk-card-default">
        <img src="{{asset("assets/images/aboutus2.jpg")}}" class="w-100" alt="">
        </div>
    </div>
</div>
@endsection