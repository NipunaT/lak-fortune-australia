
@extends('layouts.app')
@section('title','Home Page')
@section('maincarousel','hidden')
@section('content')
<div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
    <div class="uk-card-media-left uk-cover-container">
<img src="{{asset("assets/images/404.svg")}}" alt="" uk-cover>
        <canvas width="600" height="600"></canvas>
    </div>
    <div>
        <div class="uk-card-body text-center mdup:mt-40">
            <h3 class="text-4xl text-red-500 font-bold">Error 404</h3>
            <p class="text-3xl">Oops ... No seals found here</p>
        </div>
    </div>
</div>
@endsection