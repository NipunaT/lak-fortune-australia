<footer>
    {{-- Large footer --}}
    <nav class="bg-white mt-4 inset-x-0 bottom-0 md:hidden justify-center  uk-height-medium" uk-navbar>
        <!-- Center -->
        <div class="uk-grid-small uk-child-width-1-4 p-4 m-auto" uk-grid>
            {{-- 1 --}}
            <div>
                <div
                    class="text-xl font-bold text-red-500 uk-padding-small uk-padding-remove-left uk-padding-remove-top text-center ">
                    Lak Fortune Australia</div>
                <p class="uk-text-justify">
                    Lak Fortune Australia Pty Ltd is one of Australia’s leading temper
                    evident seals distributor based in Melbourne Australia. We pride
                    ourselves in providing the best security seal products at the lowest
                    prices to our customers. Therefore, allowing them to maximise margins
                    and increase profits.
                </p>
            </div>
            {{-- 2 --}}
            <div>
                <div
                    class="text-md font-bold uk-padding-small uk-padding-remove-left uk-padding-remove-top center text-center">
                    Quick links
                </div>
                <ul class="uk-list uk-list-large text-center">
                    <li><a style="text-decoration: none;" href="/">
                            <i class="fas fa-caret-right mr-2"></i>Home</a></li>
                    <li><a style="text-decoration: none;" href="/aboutus"><i class="fas fa-caret-right mr-2"></i>About
                            us</a></li>
                    <li><a style="text-decoration: none;" href="/contactus"><i
                                class="fas fa-caret-right mr-2"></i>Contact
                            us</a></li>
                </ul>
            </div>
            {{-- 3 --}}
            <div class="text-left">
                <div
                    class="text-md font-bold uk-padding-small uk-padding-remove-left uk-padding-remove-top text-center">Be the first to know
                </div>
              
                <div class="uk-card uk-card-small uk-card-body">
                    <form class="uk-form-stacked" action="/subscribe" method="POST">
                        <!-- Subscribe -->
                        <div class="uk-margin uk-flex justify-center" uk-margin>
    
                            <div uk-form-custom="target: true">
                                <input class="uk-input uk-form-width-small rounded-l-full" name="semail" type="text" placeholder="Email">
                            </div>
                            <button class="uk-button bg-red-500 text-white rounded-r-full">Subscribe</button>
                        </div>
                        @csrf
                    </form>
                </div>
                @if (Session::has('subscribed'))
                    <!-- Alert successfull -->
                    <div class="uk-alert-primary text-center" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        <p>Subscribe successfuly !</p>
                    </div>
                    
                @endif
                                         

            </div>
            {{-- 4 --}}
            <div>
                <div
                    class="text-md font-bold uk-padding-small uk-padding-remove-left uk-padding-remove-top text-center">
                    Follow us
                    on</div>
                <div class="text-center">
                    <a><i class="fab fa-facebook text-xl hover:text-blue-500 cursor-pointer uk-padding-small"></i></a>
                    <a><i
                            class="fab fa-facebook-messenger text-xl hover:text-blue-500 cursor-pointer uk-padding-small"></i></a>
                    <a><i class="fab fa-instagram text-xl hover:text-blue-500 cursor-pointer uk-padding-small"></i></a>
                    <a><i class="fab fa-twitter text-xl hover:text-blue-500 cursor-pointer uk-padding-small"></i></a>
                </div>
            </div>
        </div>
    </nav>
    <hr>
    {{-- Small footer --}}
    <nav class="bg-white inset-x-0 bottom-0 md:mt-4" uk-navbar>
        <!-- Center -->
        <div class="uk-navbar-center">
            <div class="uk-navbar-center">
                <div class="uk-navbar-nav">
                    <div class="uk-nav-item m-5">
                        <div class="uk-text-small">© Lakfortune Australia , All rights reserved | Made with <i
                                class="fas fa-heart text-red-500"></i> by <a class="no-underline"
                                href="https://www.facebook.com/interpretercompany/">INTERPRETER</a></div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</footer>